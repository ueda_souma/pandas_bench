# coding=utf-8
import itertools
import pandas


def pandas_iter_with_index(df, col):
    """ :param pandas.DataFrame df: """
    for i in df.index:
        df.ix[i, col] = 1
    return df


def pandas_iter_with_series(df, col):
    """ :param pandas.DataFrame df: """
    df[col] = df[col].apply(lambda _: 1)
    return df


def np_iter(df, col):
    """ :param pandas.DataFrame df: """
    col_index = df.columns.get_loc(col)
    dfn = df.as_matrix()
    for i in xrange(df.shape[0]):
        dfn[i][col_index] = 1
    return pandas.DataFrame(dfn)


def list_iter(df, col):
    """ :param pandas.DataFrame df: """
    col_index = df.columns.get_loc(col)
    dfn = df.as_matrix().tolist()
    for i in xrange(df.shape[0]):
        dfn[i][col_index] = 1
    return pandas.DataFrame(dfn)
