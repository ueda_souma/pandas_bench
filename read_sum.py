# coding=utf-8
import itertools

import numpy
import pandas


def pandas_sum_with_index(df, col):
    """ :param pandas.DataFrame df: """
    total = 0
    for i in df.index:
        total += df.ix[i, col]
    return total


def pandas_sum_with_series(df, col):
    """ :param pandas.DataFrame df: """
    total = 0
    for i in df[col]:
        total += i
    return total


def pandas_smart_sum(df, col):
    """ :param pandas.DataFrame df: """
    return df[col].sum()


def pandas_iter_rows(df, col):
    """ :param pandas.DataFrame df: """
    total = 0
    for _, row in df.iterrows():
        total += row[col]
    return total


def pandas_iter_tuples(df, col):
    """ :param pandas.DataFrame df: """
    total = 0
    col_index = df.columns.get_loc(col)
    for row in df.itertuples(index=False):
        total += row[col_index]
    return total


def np_sum(df, col):
    return numpy.sum(df[col])


def np_iter(df, col):
    """ :param pandas.DataFrame df: """
    col_index = df.columns.get_loc(col)
    dfn = df.as_matrix()
    total = 0
    for i in xrange(df.shape[0]):
        total += dfn[i][col_index]
    return total


def list_sum(df, col):
    return sum(df[col])


def list_iter(df, col):
    """ :param pandas.DataFrame df: """
    col_index = df.columns.get_loc(col)
    dfn = df.as_matrix().tolist()
    total = 0
    for i in xrange(df.shape[0]):
        total += dfn[i][col_index]
    return total
