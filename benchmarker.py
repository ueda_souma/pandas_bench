import gc
import sys
import time

import numpy
import pandas

import read_sum
import rewrite_all_col
import rewrite_single_col


def make_dataframe(h, w):
    return pandas.DataFrame(numpy.zeros([h, w]))


def inner_bench(iteration, df, f):
    start = time.time()
    for _ in xrange(iteration):
        f(df, 10)
    end = time.time()
    return float((end - start) * 1000) / iteration


def outer_bench(iteration, f, run_a3=True):
    """
    :param int iteration:
    :param (DataFrame, _)->Any f:
    """
    gc.collect()
    sys.stderr.write("\r\033[Krunning 100x100")
    a1 = inner_bench(iteration, make_dataframe(100, 100), f)
    sys.stderr.write("\r\033[Krunning 10kx100")
    a2 = inner_bench(iteration, make_dataframe(10000, 100), f)
    if run_a3:
        sys.stderr.write("\r\033[Krunning 10kx10k")
        a3 = inner_bench(iteration // 10, make_dataframe(10000, 10000), f)
        sys.stderr.write("\r\033[K")
        print("{:>31}\t{:>12.3f}{:>12.3f}{:>12.3f}".format(f.__name__, a1, a2, a3))
    else:
        sys.stderr.write("\r\033[K")
        print("{:>31}\t{:>12.3f}{:>12.3f}{:>12}".format(f.__name__, a1, a2, '-'))


print("{:>31}\t{:>12}{:>12}{:>12}".format('name', '100x100', '10kx100', '10kx10k'))
print('----- rewrite all columns -----')
outer_bench(1, rewrite_all_col.pandas_iter_with_index, False)
outer_bench(100, rewrite_all_col.pandas_iteritems)
outer_bench(100, rewrite_all_col.pandas_iteritems_concat)
outer_bench(100, rewrite_all_col.np_iter)
outer_bench(100, rewrite_all_col.list_iter)
print('----- rewrite single column -----')
outer_bench(100, rewrite_single_col.pandas_iter_with_index)
outer_bench(1000, rewrite_single_col.pandas_iter_with_series)
outer_bench(1000, rewrite_single_col.np_iter)
outer_bench(100, rewrite_single_col.list_iter)
print('----- read single column ------')
outer_bench(1000, read_sum.pandas_sum_with_index)
outer_bench(10000, read_sum.pandas_sum_with_series)
outer_benprch(10000, read_sum.pandas_smart_sum)
outer_bench(1000, read_sum.pandas_iter_rows)
outer_bench(1000, read_sum.pandas_iter_tuples)
outer_bench(10000, read_sum.np_iter)
outer_bench(10000, read_sum.np_sum)
outer_bench(100, read_sum.list_iter)
outer_bench(100, read_sum.list_sum)
