某所でpandasのiterationが遅いと言われてたのでべんちまーく。

結論で言えば、pandasのイテレーションはおそくない。

t2.largeでの実行結果

```
                           name	     100x100     10kx100     10kx10k
----- rewrite all columns -----
         pandas_iter_with_index	    1576.191  159105.073           -
               pandas_iteritems	      41.042     309.055   60536.653
        pandas_iteritems_concat	      17.138     290.167   29265.264
                        np_iter	       2.899     276.972   27832.843
                      list_iter	       8.181     238.669   28001.284
----- rewrite single column -----
         pandas_iter_with_index	      16.104    1587.059    1940.991
        pandas_iter_with_series	       0.296       2.858      27.970
                        np_iter	       0.149       2.777       2.877
                      list_iter	       3.906     149.439   19249.486
----- read single column ------
          pandas_sum_with_index	       0.688      69.443      69.187
         pandas_sum_with_series	       0.025       1.341       1.351
               pandas_smart_sum	       0.061       0.141       0.161
               pandas_iter_rows	       4.290     421.451     421.697
             pandas_iter_tuples	      12.812      65.350    6693.855
                        np_iter	       0.050       3.407       3.458
                         np_sum	       0.064       0.145       0.156
                      list_iter	       0.172      35.285    3894.125
                       list_sum	       0.024       1.057       1.141
```
