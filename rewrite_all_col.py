# coding=utf-8
import itertools
import pandas


def pandas_iter_with_index(df, _):
    """ :param pandas.DataFrame df: """
    for c in df.columns:
        for i in df.index:
            df.ix[i, c] = 1
    return df


def pandas_iteritems(df, _):
    """ :param pandas.DataFrame df: """
    new_df = pandas.DataFrame()
    for name, col in df.iteritems():
        #: :type col: pandas.Series
        new_col = col.apply(lambda _: 1)
        new_df[name] = new_col
    return new_df


def pandas_iteritems_concat(df, _):
    """ :param pandas.DataFrame df: """
    new_cols = [col.apply(lambda _: 1) for _, col in df.iteritems()]
    return pandas.concat(new_cols, axis=1)


def np_iter(df, _):
    """ :param pandas.DataFrame df: """
    dfn = df.as_matrix()
    for i, j in itertools.product(range(df.shape[0]), range(df.shape[1])):
        dfn[i][j] = 1
    return pandas.DataFrame(dfn)


def list_iter(df, _):
    """ :param pandas.DataFrame df: """
    dfn = df.as_matrix().tolist()
    for i, j in itertools.product(range(df.shape[0]), range(df.shape[1])):
        dfn[i][j] = 1
    return pandas.DataFrame(dfn)
